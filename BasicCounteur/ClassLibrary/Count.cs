﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Count
    {
       int counter=0;

        public  int Increment()
        {
            counter = counter + 1;
            return counter;
        } 

        public  int Decrement()
        {
            counter = counter - 1;
            if (counter <= 0)
                counter = 0;

            return counter;
        }

        public  int Raz()
        {
            counter = 0;
            return counter;
        }



    }
}
