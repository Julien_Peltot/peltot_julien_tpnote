﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Counter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Bdn_moins = New System.Windows.Forms.Button()
        Me.Bdn_plus = New System.Windows.Forms.Button()
        Me.bdn_raz = New System.Windows.Forms.Button()
        Me.Lbl_nombre = New System.Windows.Forms.Label()
        Me.Lbl_total = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Bdn_moins
        '
        Me.Bdn_moins.Location = New System.Drawing.Point(46, 140)
        Me.Bdn_moins.Name = "Bdn_moins"
        Me.Bdn_moins.Size = New System.Drawing.Size(174, 71)
        Me.Bdn_moins.TabIndex = 0
        Me.Bdn_moins.Text = "-"
        Me.Bdn_moins.UseVisualStyleBackColor = True
        '
        'Bdn_plus
        '
        Me.Bdn_plus.Location = New System.Drawing.Point(578, 140)
        Me.Bdn_plus.Name = "Bdn_plus"
        Me.Bdn_plus.Size = New System.Drawing.Size(172, 71)
        Me.Bdn_plus.TabIndex = 1
        Me.Bdn_plus.Text = "+"
        Me.Bdn_plus.UseVisualStyleBackColor = True
        '
        'bdn_raz
        '
        Me.bdn_raz.Location = New System.Drawing.Point(269, 271)
        Me.bdn_raz.Name = "bdn_raz"
        Me.bdn_raz.Size = New System.Drawing.Size(237, 71)
        Me.bdn_raz.TabIndex = 2
        Me.bdn_raz.Text = "RAZ"
        Me.bdn_raz.UseVisualStyleBackColor = True
        '
        'Lbl_nombre
        '
        Me.Lbl_nombre.AutoSize = True
        Me.Lbl_nombre.Location = New System.Drawing.Point(388, 167)
        Me.Lbl_nombre.Name = "Lbl_nombre"
        Me.Lbl_nombre.Size = New System.Drawing.Size(16, 17)
        Me.Lbl_nombre.TabIndex = 3
        Me.Lbl_nombre.Text = "0"
        '
        'Lbl_total
        '
        Me.Lbl_total.AutoSize = True
        Me.Lbl_total.Location = New System.Drawing.Point(369, 98)
        Me.Lbl_total.Name = "Lbl_total"
        Me.Lbl_total.Size = New System.Drawing.Size(40, 17)
        Me.Lbl_total.TabIndex = 4
        Me.Lbl_total.Text = "Total"
        '
        'Counter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Lbl_total)
        Me.Controls.Add(Me.Lbl_nombre)
        Me.Controls.Add(Me.bdn_raz)
        Me.Controls.Add(Me.Bdn_plus)
        Me.Controls.Add(Me.Bdn_moins)
        Me.Name = "Counter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Bdn_moins As Button
    Friend WithEvents Bdn_plus As Button
    Friend WithEvents bdn_raz As Button
    Friend WithEvents Lbl_nombre As Label
    Friend WithEvents Lbl_total As Label
End Class
